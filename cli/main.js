#!/usr/bin/env node
process.env.BABEL_ENV = "production";

const path = require("path");
const { red } = require("chalk");
const fs = require("fs-extra");
const { execSync } = require("child_process");
const { transformFileAsync } = require("@babel/core");
const manageTranslations = require("../dist").default;

const babelPluginReactIntl = path.join(__dirname, "../dist/plugin");

const JS_LIKE_FILE = /\.((t|j)sx?|svelte|vue|html)$/;

const cwd = process.cwd();

const anyUncommitedChanges = execSync("git status --porcelain", { cwd })
  .toString()
  .replace("\n", "");
if (anyUncommitedChanges && !process.argv.includes("--force")) {
  // this script deletes redundant items from the json files
  // so we make sure that the user won't lose any work.
  console.error(
    red(
      "-\n-\n-\n❌ There are uncommitted git changes! Please commit your changes and re-run this script, or use --force\n-\n-\n-"
    )
  );
  console.error(anyUncommitedChanges);
  process.exit(1);
}

// eslint-disable-next-line import/no-dynamic-require
const pkg = require(path.join(cwd, "package.json"));

if (!pkg.i18n) {
  console.error(red("Package.json does not include i18n config"));
  process.exit(1);
}
const config = pkg.i18n;

const moduleSourceName = config.moduleSourceName || "i18n";
const relativeMsgDir = config.messagesDir || ".messages";
const fullMessagesDir = path.join(cwd, relativeMsgDir);
const fullLocaleFilesDir = path.join(cwd, config.localeDir);
const fullSourceFolder = path.join(cwd, config.sourceFolder || "src");
const codeQualityReport = path.join(cwd, config.codeQualityReport);

// major hack to speed up babel and avoid parsing every file
const MATCHES = [
  `from "${moduleSourceName}";`,
  `import("${moduleSourceName}")`,
  `import('${moduleSourceName}')`,
  "// @i18nForceCheck",
];

/**
 * @param {string} dir
 * @returns {Promise<string[]>}
 */
async function walk(dir) {
  const list = await fs.readdir(dir);
  const allFileNames = await Promise.all(
    list.map(async (fileName) => {
      if (fileName === "node_modules") return [];
      const file = path.resolve(dir, fileName);
      const stat = await fs.stat(file);
      if (stat.isDirectory()) return walk(file);
      return [file];
    })
  );
  return allFileNames.flat(Infinity);
}

/** @param {string[]} allFileNames */
async function filterFiles(allFileNames) {
  const promises = await Promise.all(
    allFileNames.map(async (fullPath) => {
      if (config.moduleSourceName === false) {
        // if the user wants to seach any file, then we'll include the file if it's a source file
        return fullPath.match(JS_LIKE_FILE) && fullPath;
      } else {
        // if the user has specified a moduleSourceName, then only include the file if it imports the moduleSourceName
        const fileContent = (await fs.readFile(fullPath)).toString();
        return MATCHES.some((MATCH) => fileContent.includes(MATCH)) && fullPath;
      }
    })
  );

  return promises.filter(Boolean);
}

/** @param {string[]} i18nFiles */
async function babelLocalizedFiles(i18nFiles) {
  await Promise.all(
    i18nFiles.map(async (filePath) => {
      await transformFileAsync(filePath, {
        cwd,
        presets: ["react-app"],
        envName: "production",
        plugins: [
          [
            babelPluginReactIntl,
            {
              moduleSourceName,
              additionalComponentNames: ["$"],
              extractFromFormatMessageCall: true,
              messagesDir: relativeMsgDir,
            },
          ],
        ],
      });
    })
  );
}

async function callManager() {
  const languages = (await fs.readdir(fullLocaleFilesDir))
    .filter((x) => !x.startsWith("whitelist_"))
    .map((x) => x.replace(".json", ""));

  manageTranslations({
    messagesDirectory: fullMessagesDir,
    translationsDirectory: fullLocaleFilesDir,
    codeQualityReport,
    languages,
    fullTxDir: fullLocaleFilesDir,
  });
}

async function main() {
  await fs.remove(fullMessagesDir);

  const allFileNames = await walk(fullSourceFolder);
  const i18nFiles = await filterFiles(allFileNames);
  console.log(
    config.moduleSourceName
      ? `Processing ${i18nFiles.length} internationalized files...\n`
      : `Processing all files... (${i18nFiles.length})\n`
  );

  await babelLocalizedFiles(i18nFiles);

  await callManager();
}

main();
