import { D } from "./types";

export default (a: D, b: D): -1 | 0 | 1 => {
  const ka = a.key;
  const kb = b.key;

  if (ka < kb) {
    return -1;
  }
  if (ka > kb) {
    return 1;
  }
  return 0;
};
