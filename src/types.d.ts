export type CodeQuality = {
  description: string;
  fingerprint: string;
  severity: "info" | "minor" | "major" | "critical" | "blocker";
  location: {
    path: string;
    lines: {
      begin: number;
    };
  };
};

export type D = {
  key: string;
  message: string;
};
