import { D } from "./types";

type Result = {
  added: D[];
  untranslated: D[];
  deleted: D[];
  /** what gets written to disk */
  fileOutput: Record<string, string | null | undefined>;
  whitelistOutput: string[];
};

export const getCleanReport = (): Result => ({
  added: [],
  untranslated: [],
  deleted: [],
  fileOutput: {},
  whitelistOutput: [],
});

export const getLanguageReport = (
  defaultMessages: Record<string, string>,
  languageMessages: Record<string, string> = {},
  languageWhitelist: unknown[] = [],
  lang?: string
): Result => {
  const result = getCleanReport();

  const defaultMessageKeys = Object.keys(defaultMessages);

  defaultMessageKeys.forEach((key) => {
    const oldMessage = languageMessages[key];
    const defaultMessage = defaultMessages[key];

    if (oldMessage) {
      result.fileOutput[key] = oldMessage;

      if (oldMessage === defaultMessage) {
        if (languageWhitelist.indexOf(key) === -1) {
          result.untranslated.push({
            key,
            message: defaultMessage,
          });
        } else {
          result.whitelistOutput.push(key);
        }
      }
    } else {
      // hack because undefined won't show in JSON but null will.
      // so en-US should not show placeholder/null items
      result.fileOutput[key] =
        defaultMessage || (lang?.includes("en-US") ? undefined : null);

      result.added.push({
        key,
        message: defaultMessage,
      });
    }
  });

  // if the key is still in the language file but no longer in the
  // defaultMessages file, then the key was deleted.
  result.deleted = Object.keys(languageMessages)
    .filter((key) => defaultMessageKeys.indexOf(key) === -1)
    .map((key) => ({
      key,
      message: languageMessages[key],
    }));

  return result;
};
