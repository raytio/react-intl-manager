// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import stableStringify from "json-stable-stringify";
import compareByKey from "./compareByKey";

export const stringify = (
  value: any,
  { space = 2, sortKeys = true }: { space: number; sortKeys: boolean }
): string =>
  `${
    sortKeys
      ? stableStringify(value, { replacer: null, space, cmp: compareByKey })
      : JSON.stringify(value, null, space)
  }\n`;
