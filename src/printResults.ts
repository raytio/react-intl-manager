import chalk from "chalk";
import { createHash } from "crypto";
import path from "path";
import compareByKey from "./compareByKey";
import { CodeQuality, D } from "./types";

const hash = (str: string): string =>
  createHash("md5").update(str).digest("hex");

type Props = {
  deleted: D[];
  untranslated: D[];
  added: D[];
  sortKeys: boolean;
  lang: string;
  fullTxDir: string;
};

export const printResults = ({
  deleted,
  untranslated,
  added,
  sortKeys = true,
  lang,
  fullTxDir,
}: Props): CodeQuality[] => {
  const codeQuality: CodeQuality[] = [];

  const relativeFilePath = path.relative(
    process.cwd(),
    path.join(fullTxDir, lang)
  );

  if (deleted.length) {
    const items = sortKeys ? deleted.sort(compareByKey) : deleted;

    codeQuality.push(
      ...items.map(
        ({ key }): CodeQuality => ({
          severity: "blocker",
          description: `🌏 ${key} is redundant. Please remove from ${lang.replace(
            ".json",
            ""
          )}`,
          fingerprint: hash(`${lang}-${key}-deleted`),
          location: {
            path: relativeFilePath,
            lines: {
              begin: 1,
            },
          },
        })
      )
    );
  }

  const anyAdded = added.length && !lang.includes("en-US");

  if (anyAdded) {
    const items = sortKeys ? added.sort(compareByKey) : added;

    codeQuality.push(
      ...items.map(
        ({ key }): CodeQuality => ({
          severity: "minor",
          description: `🌏 ${key} is not translated into ${lang.replace(
            ".json",
            ""
          )}`,
          fingerprint: hash(`${lang}-${key}-added`),
          location: {
            path: relativeFilePath,
            lines: {
              begin: 1,
            },
          },
        })
      )
    );
  }

  const issues = [
    anyAdded && `${added.length} untranslated`,
    deleted.length && `${deleted.length} deleted`,
    untranslated.length && `${untranslated.length} untranslated`, // does't happen for us
  ].filter((x): x is string => !!x);

  const issueStr = issues.map((str) => chalk.red(str)).join(", ");

  if (lang.includes("en-US")) console.log(lang, chalk.cyan("special"));
  else console.log(lang, issueStr || chalk.green("OK"));

  return codeQuality;
};
