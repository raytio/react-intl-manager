import { readFileSync, writeFileSync } from "fs";
import { outputJSONSync } from "fs-extra";
import { sync as mkdirpSync } from "mkdirp";
import Path from "path";
import { yellow, red } from "chalk";

import readMessageFiles from "./readMessageFiles";
import createSingleMessagesFile from "./createSingleMessagesFile";
import { printResults } from "./printResults";
import { stringify } from "./stringify";

import core from "./core";

const CREATE_WHITELIST = false;

export default ({
  messagesDirectory,
  translationsDirectory,
  fullTxDir,
  whitelistsDirectory = translationsDirectory,
  codeQualityReport,
  languages = [],
  singleMessagesFile = false,
  sortKeys = true,
}) => {
  if (!messagesDirectory || !translationsDirectory) {
    throw new Error("messagesDirectory and translationsDirectory are required");
  }

  const stringifyOpts = {
    space: 2,
    sortKeys,
  };

  const defaultCoreMethods = {
    provideExtractedMessages: () => readMessageFiles(messagesDirectory),

    outputSingleFile: (combinedFiles) => {
      if (singleMessagesFile) {
        createSingleMessagesFile({
          messages: combinedFiles,
          directory: translationsDirectory,
          sortKeys,
        });
      }
    },

    outputDuplicateKeys: (duplicateIds) => {
      if (duplicateIds.length) {
        console.warn(
          yellow("Duplicates found! Please manually fix the following:"),
          duplicateIds
        );
      }
    },

    beforeReporting: () => {
      mkdirpSync(translationsDirectory);
      mkdirpSync(whitelistsDirectory);
    },

    provideLangTemplate: (lang) => {
      const languageFilename = `${lang}.json`;
      const languageFilepath = Path.join(
        translationsDirectory,
        languageFilename
      );
      const whitelistFilename = `whitelist_${lang}.json`;
      const whitelistFilepath = Path.join(
        whitelistsDirectory,
        whitelistFilename
      );

      return {
        lang,
        languageFilename,
        languageFilepath,
        whitelistFilename,
        whitelistFilepath,
      };
    },

    provideTranslationsFile: (langResults) => {
      try {
        return JSON.parse(readFileSync(langResults.languageFilepath));
      } catch {
        return undefined;
      }
    },

    provideWhitelistFile: (langResults) => {
      try {
        return JSON.parse(readFileSync(langResults.whitelistFilepath));
      } catch {
        return undefined;
      }
    },

    reportLanguage: (langResults) => {
      if (
        !langResults.report.noTranslationFile &&
        !langResults.report.noWhitelistFile
      ) {
        const codeQuality = printResults({
          ...langResults.report,
          sortKeys,
          lang: langResults.languageFilename,
          fullTxDir,
        });

        writeFileSync(
          langResults.languageFilepath,
          stringify(langResults.report.fileOutput, stringifyOpts)
        );
        if (CREATE_WHITELIST) {
          writeFileSync(
            langResults.whitelistFilepath,
            stringify(langResults.report.whitelistOutput, stringifyOpts)
          );
        }
        // TODO: temp hack to only generate code quality report for en.json
        return langResults.languageFilename === "en.json" ? codeQuality : [];
      }

      if (langResults.report.noTranslationFile) {
        console.warn(
          yellow(`${langResults.languageFilename} not found; new one created.`)
        );
        writeFileSync(
          langResults,
          stringify(langResults.report.fileOutput, stringifyOpts)
        );
      }

      if (langResults.report.noWhitelistFile && CREATE_WHITELIST) {
        writeFileSync(
          langResults.whitelistFilepath,
          stringify([], stringifyOpts)
        );
      }

      return [];
    },

    afterReporting: (codeQualityList) => {
      // save codeQuality
      if (codeQualityReport) {
        let existing = [];
        try {
          existing = JSON.parse(readFileSync(codeQualityReport));
        } catch {
          console.log(red("Could not find an existing code-quality report"));
        }
        outputJSONSync(codeQualityReport, [
          ...existing,
          ...codeQualityList.flat(),
        ]);
      }

      // return exit code 1 if any fatal issues
      return codeQualityList.flat().some((x) => x.severity === "blocker")
        ? 1
        : 0;
    },
  };

  const exitCode = core(languages, defaultCoreMethods);

  if (exitCode === 1 && process.env.CI) {
    console.error(
      red(
        "FAILED: i18n validation detected some fatal issues. Run `yarn i18n` locally and commit the changes."
      )
    );
  }

  process.exit(exitCode);
};
