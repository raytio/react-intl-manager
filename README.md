# react-intl-manager

![pipeline](https://gitlab.com/raytio/react-intl-manager/badges/main/pipeline.svg?style=flat-square)
![coverage](https://gitlab.com/raytio/react-intl-manager/badges/main/coverage.svg?style=flat-square)
![npm version](https://img.shields.io/npm/v/@raytio/react-intl-manager?color=%234c1&style=flat-square)

A fork of [react-intl-translations-manager](https://github.com/GertjanReynaert/react-intl-translations-manager) which works better for Raytio and integrates with the GitLab CI.

What it actually does

- Remove duplicates from the locale json files
- Remove redundant keys from locale json files
- Detect missing keys and add them as `null`
- Generate a codequality report for codeclimate or GitLab CI.

Note that for performance reasons, this script only parses files that import the `i18n` module. If it fails to detect this, you can add `// @i18nForceCheck` to force it to check that file.

## Usage:

```sh
npm i -D @raytio/react-intl-manager
```

Then add a section to package.json called `i18n` which contains the config:

```jsonc
{
  "scripts": {
    "i18n": "react-intl-manager"
  },
  "i18n": {
    "localeDir": "path/to/locale-json-files",

    // optional, src by default
    "sourceFolder": "src",

    // optional, .messages by default
    "messagesDir": ".messages",

    // optional, the name of the es module - default is "i18n", not "react-intl"
    // set to `false` to parse every js-like file
    "moduleSourceName": "i18n",

    // optional, this will append any issues to this file if exists (or create it)
    "codeQualityReport": "path/to/codequality.json"
  }
}
```

## Contributing

```sh
# makes cli.js available
npm link
```

## Publishing

The CI will publish every commit to `main` if the version number in package.json has been bumped.
