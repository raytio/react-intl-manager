# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## 6.4.0 (2023-11-09)

- !8 Add `babel-preset-react-app` to the dependencies

## 6.3.1 (2023-02-20)

- !7 Better error message when there are uncommitted git changes

## 6.3.0 (2022-04-02)

- !6 Support searching all source files, not just those that import the `moduleSourceName`

## 6.2.0 (2021-04-10)

- !5 use `severity` in the gitlab codequality report

## 6.1.2 (2020-08-28)

- !4 various fixes

## 6.1.1 (2020-08-18)

- !3 improve i18n import detection. Also refuse to run if dirty git tree

## 6.1.0 (2020-08-18)

- !2 Fixes and simplify usage

## 6.0.2 (2020-08-06)

- !1 improve docs and use config file
- 3d4e2216 moved to `@raytio/` scoped package
- 3d0bb4de moved to gitlab

## 6.0.1 (2020-08-05)

- 812e71af append to existing code quality report if there is one
- 45042e9d fix bug

## 6.0.0 (2020-08-05)

- 1545016e First stable release
